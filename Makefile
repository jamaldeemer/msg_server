CC = g++
CFLAGS = -g -Wall -std=c++11 
LDFLAGS = -L/opt/local/lib -lboost_system -pthread -lboost_thread
IFLAGS = -I/usr/include/boost


#### Common ###############################################

default: msg_server msg_client

clean: 
	$(RM) count *.o *~ msg_server msg_client

#### Message Server #######################################

# Executable binaries
msg_server: msg_server_main.o
	$(CC) $(CFLAGS) -o msg_server msg_server_main.o $(LDFLAGS) $(IFLAGS)

# Object Files 
msg_server_main.o: msg_server_main.cpp 
	$(CC) $(CFLAGS) -c msg_server_main.cpp $(LDFLAGS) $(IFLAGS)

#### Message Client #######################################

msg_client: msg_client.o msg_client_main.o
	$(CC) $(CFLAGS) -o msg_client msg_client_main.o msg_client.o $(LDFLAGS) $(IFLAGS)

msg_client_main.o: msg_client_main.cpp 
	$(CC) $(CFLAGS) -c msg_client_main.cpp $(LDFLAGS) $(IFLAGS)

msg_client.o: msg_client.cpp msg_client.h
	$(CC) $(CFLAGS) -c msg_client.cpp $(LDFLAGS) $(IFLAGS)


