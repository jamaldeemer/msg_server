//
// msg_client.cpp
// ~~~~~~~~~~~~~~~

#include <cstdlib>
#include <deque>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include "chat_message.hpp"
#include "msg_client.h"

using boost::asio::ip::tcp;

typedef std::deque<chat_message> chat_message_queue;

// Public Members 

msg_client::msg_client(boost::asio::io_service& io_service, tcp::resolver::iterator endpoint_iterator) : io_service_(io_service), socket_(io_service) {
  std::cout << "msg_client::msg_client()" << std::endl; 
  boost::asio::async_connect(socket_, endpoint_iterator, boost::bind(&msg_client::handle_connect, this, boost::asio::placeholders::error));
}

void msg_client::write(const chat_message& msg) {
  std::cout << "msg_client::write()" << std::endl; 
  io_service_.post(boost::bind(&msg_client::do_write, this, msg));
}

void msg_client::close() {
  std::cout << "msg_client::close()" << std::endl; 
  io_service_.post(boost::bind(&msg_client::do_close, this));
}


// Private Members

void msg_client::handle_connect(const boost::system::error_code& error) {
  std::cout << "msg_client::handle_connect()" << std::endl; 
  if (!error) {
    boost::asio::async_read(socket_, boost::asio::buffer(read_msg_.data(), chat_message::header_length), boost::bind(&msg_client::handle_read_header, this, boost::asio::placeholders::error));
  }
}

void msg_client::handle_read_header(const boost::system::error_code& error) {
  std::cout << "msg_client::handle_read_header()" << std::endl; 
  if (!error && read_msg_.decode_header()) {
    boost::asio::async_read(socket_, boost::asio::buffer(read_msg_.body(), read_msg_.body_length()), boost::bind(&msg_client::handle_read_body, this, boost::asio::placeholders::error));
  } else {
    do_close();
  }
}

void msg_client::handle_read_body(const boost::system::error_code& error) {
  std::cout << "msg_client::handle_read_body()" << std::endl; 
  if (!error) {

    std::cout.write(read_msg_.body(), read_msg_.body_length());
    std::cout << "\n";

    boost::asio::async_read(socket_, boost::asio::buffer(read_msg_.data(), chat_message::header_length), boost::bind(&msg_client::handle_read_header, this, boost::asio::placeholders::error));
  } else {
    do_close();
  }
}

void msg_client::do_write(chat_message msg) {
  std::cout << "msg_client::do_body()" << std::endl; 
  bool write_in_progress = !write_msgs_.empty();

  write_msgs_.push_back(msg);

  if (!write_in_progress) {
    boost::asio::async_write(socket_, boost::asio::buffer(write_msgs_.front().data(), write_msgs_.front().length()), boost::bind(&msg_client::handle_write, this, boost::asio::placeholders::error));
  }
}

void msg_client::handle_write(const boost::system::error_code& error) {
  std::cout << "msg_client::handle_write()" << std::endl; 
  if (!error) {

    write_msgs_.pop_front();

    if (!write_msgs_.empty()) {
      boost::asio::async_write(socket_, boost::asio::buffer(write_msgs_.front().data(), write_msgs_.front().length()), boost::bind(&msg_client::handle_write, this, boost::asio::placeholders::error));
    }
  } else {
    do_close();
  }
}

void msg_client::do_close() {
  std::cout << "msg_client::do_close()" << std::endl; 
  socket_.close();
}
