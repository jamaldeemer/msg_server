//
// msg_client_main.cpp
// ~~~~~~~~~~~~~~~


#include <cstdlib>
#include <deque>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include "chat_message.hpp"
#include "msg_client.h"


using boost::asio::ip::tcp;


int main(int argc, char* argv[])
{
  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: msg_client <host> <port>\n";
      return 1;
    }

    // Initializing some Boost ASIO items
    boost::asio::io_service io_service;
    tcp::resolver resolver(io_service);
    tcp::resolver::query query(argv[1], argv[2]);
    tcp::resolver::iterator iterator = resolver.resolve(query);
    boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));


    // Constructing our chat client object
    msg_client c(io_service, iterator);

    // Buffer for our chat messages 
    char line[chat_message::max_body_length + 1];

    // Main Loop 
    while (std::cin.getline(line, chat_message::max_body_length + 1))
    {
      using namespace std; // For strlen and memcpy.
      chat_message msg;
      msg.body_length(strlen(line));
      memcpy(msg.body(), line, msg.body_length());
      msg.encode_header();
      c.write(msg);
    }

    c.close();
    t.join();

  } catch (std::exception& e) {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}