#/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
PASSWORD='rmsg1515'
PROJECTFOLDER='myproject'

mkdir /home/vagrant/rmsg

sudo apt-get install -y cmake
sudo apt-get install -y g++
sudo apt-get install -y libboost-all-dev

# GDB, for debugging
sudo apt-get install -y gdb

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo apt-get -y install mysql-server

echo "Adding some nice options to vimrc..."
echo "set tabstop=2" >> /root/.vimrc
echo "set shiftwidth=2" >> /root/.vimrc
echo "set softtabstop=2" >> /root/.vimrc
echo "set expandtab" >> /root/.vimrc
echo "set hlsearch" >> /root/.vimrc
echo "syntax on" >> /root/.vimrc
cp /root/.vimrc /home/vagrant/.vimrc

# install openssl (needed to clone from GitHub, as github is https only)
sudo apt-get -y install openssl

# install git
sudo apt-get -y install git

# final feedback
echo "Voila!"



