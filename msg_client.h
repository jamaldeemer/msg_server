// msg_client.h
// -------------

#ifndef MSG_CLIENT_H
#define MSG_CLIENT_H


#include <cstdlib>
#include <deque>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include "chat_message.hpp"

using boost::asio::ip::tcp;

typedef std::deque<chat_message> chat_message_queue;

class msg_client {
  public:
    msg_client(boost::asio::io_service&, tcp::resolver::iterator);
    void write(const chat_message&);
    void close();


  private:
    void handle_connect(const boost::system::error_code& );
    void handle_read_header(const boost::system::error_code& );
    void handle_read_body(const boost::system::error_code& error);
    void do_write(chat_message msg);
    void handle_write(const boost::system::error_code& error);
    void do_close();

    boost::asio::io_service& io_service_;
    tcp::socket socket_;
    chat_message read_msg_;
    chat_message_queue write_msgs_;

};

#endif